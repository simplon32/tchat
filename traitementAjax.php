<?php
require "init_root.php";

$_SESSION['connect'] = true;
$user = $_SESSION['user'] ;
$pseudo = $user->getPseudo();
$messagerepo = new MessagesRepository();

//si un lessage est posté
if(isset($_POST['message'])){
    //on crée une instance d'objet du message 
    $message = $messagerepo->CreateMsg((int) $user->getId(), $_POST['message']);
    //on enregistre le message en variable de session
    $_SESSION['message'] = $message;
    //on récupère tous les messages
    $tousLesMsg = $messagerepo->getAll();

    //pour chaque message on affiche dans le html la date , le pseudo,le message
    foreach($tousLesMsg as $numero=>$message){
        ?>
       
        <div class = "M_date"> <?= date('d/m/y, H:i:s',$message->getDate());?></div>
        <div id="<?= $message->getId(); ?> "

         class = "M <?php
         //si l éditeur du message est la personne connecté la div du msg sera de classe M_envoye sinon M_recu
         if($message->getPseudo() == $pseudo)
            {?>
             M_envoye">
            <?php ;}
        else {?>
        M_recu">
            <?php
        ;} ?>
        <h3> <?= $message->getPseudo();?> </h3>
        <p class="M_message"> <?=  $message->getMsg(); ?> </p>
        </div>


<?php
    }

}
?>