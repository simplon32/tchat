<?php
class LectureRepository{
   
    private $db;

// On commence par créer la connexion :

    public function __construct(){
    $this->db = new Database();
    $this->db = $this->db->getBDD();
    }

    public function getLu($Id_message, $Id_utilisateur){
        $sql="SELECT Lu FROM lecture WHERE Id_message = :idMessage AND Id_utilisateur = :idUtilisateur";
        $requeteLu = $this->db-> prepare($sql);
        $requeteLu -> execute([':idMessage'=>$Id_message, ':idUtilisateur '=> $Id_utilisateur]);
        $lu =$requeteLu ->fetch(PDO::FETCH_ASSOC);
        return $lu;
    }
}