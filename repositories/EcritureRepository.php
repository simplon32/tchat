<?php
class EcritureRepository{
    private $_db;

    // On commence par créer la connexion :
  
    public function __construct(){
      $this->_db = new Database();
      $this->_db = $this->_db->getBDD();
    }

    public function getEcrivain($IdMsg){
        $sql = "SELECT id_Utilisateur FROM ecriture WHERE id_Message = :idmsg";
        $requeteIdUser = $this->_db->prepare($sql);
        $requeteIdUser->execute([':idmsg' =>$IdMsg]);
        $idUser = $requeteIdUser ->fetch(PDO::FETCH_ASSOC);
        return $idUser['id_Utilisateur'];
    }

    public function getIdMsg($IdUser){
        $sql="SELECT id_Message FROM ecriture WHERE id_Utilisateur = :iduser";
        $requeteIdMsg = $this->_db->prepare($sql);
        $requeteIdMsg->execute([':iduser' =>$IdUser]);
        $idMsg = $requeteIdMsg ->fetch(PDO::FETCH_ASSOC);
        return $idMsg['id_Message'];
    }

//     p
}