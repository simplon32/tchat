<?php
class UtilisateursRepository{
    private $_db;

    public function __construct(){
        $this->_db = new Database;
        $this->_db = $this->_db->getBDD();
    }

    public function CreateUser(string $pseudo, string $mdp){
        $sql= 'SELECT Pseudo FROM utilisateurs WHERE Pseudo = :pseudo';
        try{
            $requetePseudo = $this->_db-> prepare($sql);
            $requetePseudo->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
            $requetePseudo -> execute();
            $pseudoPresent= $requetePseudo->fetch(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e){
            echo 'erreur'.$e->getMessage();
        }
        var_dump($pseudoPresent);
        if(!$pseudoPresent){
            $sql= "INSERT INTO utilisateurs (Pseudo, Mdp) VALUES(:pseudo, :mdp)";
            $requete = $this->_db->prepare($sql);
            var_dump($requete);
            $requete->execute([':pseudo'=>$pseudo,
                                ':mdp'=>$mdp        
                            ]);
           
            return "<p>l\'utilisateur $pseudo a été ajouté à la BDD</p>";
        }
        else if ($pseudoPresent[':pseudo']== $pseudo){
            return "l\'identifiant $pseudo est déjà utilisé.<br>";
        }
    }
    public function getUser($pseudo){
        $sql = 'SELECT * FROM Utilisateurs WHERE Pseudo = :pseudo';
        $requete = $this->_db->prepare($sql);
        $requete->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
        $requete -> execute();
        $user = $requete->fetch(PDO::FETCH_ASSOC);
        if ($user){$user = new Utilisateurs((int)$user['Id'], $user['Pseudo'],$user['Mdp'], $user['NbMsgNonLu']);
        }    
        return $user;
 
        
    }
    
    public function getId($user){
        $sql = "SELECT Id FROM utilisateurs WHERE Pseudo = :pseudo ;";
        $requete = $this->_db->prepare($sql);
        $requete->execute([':pseudo'=>$user]);
        $id = $requete->fetch(PDO::FETCH_ASSOC);
          return $id['Id'];
        }
    public function getMdp($user){
        $sql = "SELECT Mdp FROM utilisateurs WHERE Pseudo = :pseudo ;";
        $requete = $this->_db->prepare($sql);
        $requete->execute([':pseudo'=>$user]);
        $Mdp = $requete->fetch(PDO::FETCH_ASSOC);
          return $Mdp['Mdp'];
        }
    
  
}

?>