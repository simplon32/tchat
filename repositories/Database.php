<?php
class Database{
    const DB_HOST='localhost';
    const DB_BASE='tchat';
    const DB_USER='tchat';
    const DB_MDP='tchat';
    private $_BDD;

    public function __construct(){
        $this->connectBDD();
    }

    private function connectBDD(){
        try{
            $this->_BDD = new PDO("mysql:host=".self::DB_HOST.";dbname=".self::DB_BASE,self::DB_USER,self::DB_MDP, array(PDO::ATTR_ERRMODE =>PDO::ERRMODE_EXCEPTION));
            echo"BDD connectée";
        }
       catch (PDOException $e){
        echo 'Connexion échouée'. $e->getMessage();
       }
    }
    public function getBDD(){
        return $this->_BDD;
    }
    public function closeBDD(){
        $this->_BDD=NULL;
    }

    public function initialisationBDD(){
        //on vérifie si il existe des table dans la BDD sinon on execute le code SQL suivant:
            //SCRIPT SQL
    }

    public function uninstallBDD(){
        //code SQL de suppression des tables de la BDD
        $sql=  "DROP TABLE messages, utilisateurs, lecture, ecriture";
        $this->_BDD->query($sql);
        echo "toutes les tables ont été supprimées";
    }
}
?>