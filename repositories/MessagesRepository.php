<?php
class MessagesRepository{
    private $_db;

    public function __construct(){
        $this->_db = new Database;
        $this->_db = $this->_db->getBDD();
    }

    public function CreateMsg(int $Id_User, string $Msg){
        //on érifie que le message n'existe pas
        $sql= 'SELECT Msg FROM messages M, ecriture E
         WHERE Msg = :Msg
         AND M.Id = E.Id_Message';
        try{
            $requeteMsg = $this->_db->prepare($sql);
            $requeteMsg->execute([':Msg'=>$Msg]);
            $msgPresent = $requeteMsg->fetch(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e){
            echo"erreur".$e->getMessage();
        }
        //si le message n'existe pas on insère dans la table message le msg et la date actuelle
        //dans la table ecriture l'ID de l'utilisateur qui ecri le msg et l'id du message
        //dans la table lecture l'ID  de l'utilisateur qui ecri le msg et l'id du message
        if(!$msgPresent  ){
            $sql= "INSERT INTO messages (Msg, date) VALUES( :msg , :dat);
            INSERT INTO ecriture (Id_Utilisateur, Id_Message) VALUES (:Id_user, (SELECT LAST_INSERT_ID()));
            INSERT INTO lecture(Id_Utilisateur, Id_Message, Lu) VALUES (:Id_user, (SELECT LAST_INSERT_ID()), NULL);";
            try{
                $requeteMsg = $this->_db->prepare($sql);
                $requeteMsg-> execute([':msg'=> $Msg,
                                        ':dat'=>time(),
                                        ':Id_user'=>$Id_User]);
         
            }
            catch(PDOException $e){echo "erreur".$e->getMessage();}
           

        }
    }
    public function getId($msg){
        $sql="SELECT Id FROM messages WHERE Msg= :msg";
        $requeteId=$this->_db->prepare($sql);
        $requeteId->execute([':msg'=>$msg]);
        $id=$requeteId->fetch(PDO::FETCH_ASSOC);
        return $id['Id'];
    }
    public function getDateMsg($Id_message){
        $sql="SELECT Date_msg FROM message WHERE Id_message = :idMessage ";
        $requeteLu = $this->_db-> prepare($sql);
        $requeteLu -> execute([':idMessage'=>$Id_message]);
        $date =$requeteLu ->fetch(PDO::FETCH_ASSOC);
        return $date;

    }
    public function setDate($Date, int $Id_message){
        if (is_string($Date)){
            $Date=strtotime($Date);
        }
        $sql="UPDATE  messages SET date = :dateMsg WHERE Id = :idMessage ";
        $requeteDate = $this->_db-> prepare($sql);
        $requeteDate -> execute([':dateMsg'=>$Date, ':idMessage'=>$Id_message]);
        $date =$requeteDate ->fetch(PDO::FETCH_ASSOC);

        // catch(ErrorException $e){echo "erreur".$e->getMessage();}
    }

    
    public function getAll(){
        $tousLesMsg = [];
        $sql="SELECT M.Id, U.Pseudo, M.Msg, M.date
         FROM messages M, utilisateurs U, ecriture E
         WHERE M.Id = E.Id_message
         AND U.Id = E.Id_Utilisateur
         ORDER BY M.date;
         ";

//l'insertion dans la table lecture n'ayant pas marche j'ai enleve les données la concernant mais garde en commentaire le code l'incluant


        // $sql="SELECT M.Msg, M.date, U.Pseudo, L.lu
        // FROM messages M, utilisateurs U, ecriture E
        // WHERE M.Id = E.Id_message
        // AND U.Id = E.Id_Utilisateur
        // AND M.Id = L.Id_message
        // AND U.Id = L.Id_Utilisateur;
        // ";
        $requete = $this->_db->query($sql);  
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
        foreach($resultat as $ligne => $valeurs){
            
            $tousLesMsg[$ligne] = new Messages($valeurs);
        }
        return $tousLesMsg;
    }
    // public function getMsg($idMsg)
}
?>