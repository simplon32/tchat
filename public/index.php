<?php
require "../init.php";
require "../connexion.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connection Tchat</title>
</head>

<body>
	<div class="connection">
		<h2>Connecter vous au Tchat</h2>

		<form action="#" method="POST" class="form-connect" id="form-inscrire">
			<label for="pseudo">Votre pseudo</label>
			<input type="text" name="pseudo" placeholder="pseudo" required /><br>

			<label for="password">Mot de passe</label>
			<input type="password" id="password" name="password" placeholder="mot de passe" required /><br>
			<input type="submit" value="Valider">
		</form>
		<p>Vous n'ëtes pas encore inscris?</p>
		<a href = "inscription.php" >Inscrivez-vous ici</a>
	</div>
</body>

</html>