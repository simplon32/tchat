<?php
class Messages{
    private $_Id, $_Pseudo, $_Msg, $_Date, $_Lu;

    public function __construct(array $infos){
        $this->hydrate($infos);
    }
   private function hydrate(array $infos){
    foreach ($infos as $key=>$value){
        $method = "set".ucfirst($key);
        if(method_exists($this, $method)){
            $this ->$method($value);
        }
    }
   }

   //getters et setters
   public function getId(){
    return $this->_Id;
   }
   public function setId($ID){
     $this->_Id=$ID;
   }

   public function getDate(){
    return $this->_Date;
   }
   public function setDate($Date){
     $this->_Date= $Date;
   }

   public function getMsg(){
    return $this->_Msg;
   }
   public function setMsg($Msg){
     $this->_Msg = $Msg;
   }

   public function getLu(){
    return $this->_Lu;
   }
   public function setLu($Lu){
     $this->_Lu=$Lu;
   }

   public function setPseudo($pseudo){
      $this->_Pseudo = $pseudo;
   }
   public function getPseudo(){
    return $this->_Pseudo;
   }
}
?>