<?php
class Utilisateurs{
    private $_Id;
    private $_Pseudo;
    private $_Mdp;
    private $_NbMsgNonLu;

    public function __construct(int $Id, string $pseudo,string $mdp, $nbMsgNonLu=NULL ){
        $this->_Pseudo = $pseudo;
        $this->_Mdp = $mdp;
        $this->_Id = $Id;

    }

    public function getId()
    {
        return $this->_Id;
    }

    public function setId($Id)
    {
        $this->_Id = $Id;

    }

    public function getPseudo()
    {
        return $this->_Pseudo;
    }

    public function setPseudo($newPseudo)
    {
        $this->_Pseudo = $newPseudo;

    }
    

    public function getMdp()
    {
        return $this->_Mdp;
    }

 
    public function setMdp($newMdp)
    {
        $this->_Mdp = $newMdp;

    }
    public function ecrireMsg($Msg){
        $message = new Messages($this->_Id, $Msg , time());
    }
}