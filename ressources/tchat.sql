------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Utilisateurs
#------------------------------------------------------------

CREATE TABLE Utilisateurs(
        Id         Int  Auto_increment  NOT NULL ,
        Pseudo     Varchar (50) NOT NULL ,
        Mdp        Varchar (50) NOT NULL ,
        NbMsgNonLu Int NOT NULL
	,CONSTRAINT Utilisateurs_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Messages
#------------------------------------------------------------

CREATE TABLE Messages(
        Id      Int  Auto_increment  NOT NULL ,
        Message Mediumtext NOT NULL ,
         Date    Datetime NOT NULL
	,CONSTRAINT Messages_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Ecriture
#------------------------------------------------------------

CREATE TABLE Ecriture(
        Id_message      Int NOT NULL ,
        Id_Utilisateurs Int NOT NULL 
	,CONSTRAINT Ecriture_PK PRIMARY KEY (Id_message,Id_Utilisateurs)

	,CONSTRAINT Ecriture_Messages_FK FOREIGN KEY (Id_message) REFERENCES Messages(Id)
	,CONSTRAINT Ecriture_Utilisateurs0_FK FOREIGN KEY (Id_Utilisateurs) REFERENCES Utilisateurs(Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Lecture
#------------------------------------------------------------

CREATE TABLE Lecture(
        Id_message      Int NOT NULL ,
        Id_Utilisateurs Int NOT NULL ,
        lu              Bool NOT NULL
	,CONSTRAINT Lecture_PK PRIMARY KEY (Id_message,Id_Utilisateurs)

	,CONSTRAINT Lecture_Messages_FK FOREIGN KEY (Id_message) REFERENCES Messages(Id)
	,CONSTRAINT Lecture_Utilisateurs0_FK FOREIGN KEY (Id_Utilisateurs) REFERENCES Utilisateurs(Id)
)ENGINE=InnoDB;



